import axios from 'axios'

export default {
    state: {
        countries: [],
        cities: []
    },
    mutations: {
        updateCountries(state, countries) {
            state.countries = countries
        },
        updateCities(state, cities) {
            state.cities = cities
        }
    },
    actions: {
        async fetchLocations({dispatch, commit}) {
            try {
                let countries = await axios.get(`https://aviasales-api.herokuapp.com/countries`)
                let cities = await axios.get(`https://aviasales-api.herokuapp.com/cities`)

                const response = await Promise.all([
                    countries,
                    cities
                ])

                const [countriesResponse, citiesResponse] = response
                commit('updateCountries', countriesResponse.data)
                commit('updateCities', citiesResponse.data)

            } catch(e) {
                console.log(e)
                return Promise.reject(e)
            }
        }
    },
    getters: {
        allCountries(state) {
            return state.countries
        },
        allCities(state) {
            return state.cities
        },
        getCitiesByCountryCode: state => code => {
            return state.cities.filter(city => city.country_code === code)
        }
    }
}